﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Stock_information : System.Web.UI.Page
{
    dboperation ob = new dboperation();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MultiView1.SetActiveView(View1);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select * from product";
            DataGrid1.DataSource = ob.view(cmd);
            DataGrid1.DataBind();
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "update product set quantity='" + TextBox3.Text + "' where product_id='" + Label1.Text + "'";
        ob.execute(cmd);
        Response.Write("<script>alert('Stock Updated..!!')</script>");
        MultiView1.SetActiveView(View1);
        cmd.CommandText = "select * from product";
        DataGrid1.DataSource = ob.view(cmd);
        DataGrid1.DataBind();

      
    }

    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        MultiView1.SetActiveView(View2);
        TextBox1.Text = e.Item.Cells[2].Text;
        TextBox4.Text = e.Item.Cells[3].Text;
        TextBox3.Text = e.Item.Cells[4].Text;
        Label1.Text = e.Item.Cells[0].Text;
    }
}