﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="category_mngmnt.aspx.cs" Inherits="category_mngmnt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style4
        {
            height: 41px;
        }
        .style7
        {
            height: 42px;
        }
        .style9
        {
            height: 316px;
        }
        .style11
        {
            width: 214px;
            height: 41px;
        }
        .style12
        {
            height: 321px;
            width: 272px;
        }
        .style13
        {
            width: 272px;
        }
        .style14
        {
            width: 153px;
            height: 41px;
        }
        .style16
        {
            width: 261px;
            height: 42px;
        }
        .style17
        {
            width: 157px;
            height: 42px;
        }
        .style18
        {
            width: 157px;
            height: 43px;
        }
        .style19
        {
            width: 261px;
            height: 43px;
        }
        .style20
        {
            height: 43px;
        }
        .style21
        {
            width: 153px;
            height: 44px;
        }
        .style22
        {
            width: 214px;
            height: 44px;
        }
        .style23
        {
            height: 44px;
        }
        .style24
        {
            height: 321px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <table class="nav-justified">
                <tr>
                    <td class="style12">
                        </td>
                    <td class="style24">
                        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                            CellPadding="4" ForeColor="#333333" GridLines="None" Height="303px" 
                            onitemcommand="DataGrid1_ItemCommand" Width="612px">
                            <AlternatingItemStyle BackColor="White" />
                            <Columns>
                                <asp:BoundColumn DataField="category_id" HeaderText="Category_Id">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="category_name" HeaderText="Category_Name">
                                </asp:BoundColumn>
                                <asp:ButtonColumn HeaderText="View" Text="view"></asp:ButtonColumn>
                            </Columns>
                            <EditItemStyle BackColor="#2461BF" />
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <ItemStyle BackColor="#EFF3FB" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        </asp:DataGrid>
                    </td>
                    <td class="style24">
                    </td>
                </tr>
                <tr>
                    <td class="style13">
                        &nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <asp:Button ID="Button1" runat="server" BackColor="#66CCFF" 
                            onclick="Button1_Click" Text="Add new category" />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style13">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <table class="style1">
                <tr>
                    <td class="style21">
                        </td>
                    <td class="style22">
                        <asp:Label ID="Label2" runat="server" ForeColor="White" Text="Category Name"></asp:Label>
                    </td>
                    <td class="style23">
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="TextBox1" ErrorMessage="**" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="style14">
                        
                        &nbsp;</td>
                    <td class="style11">
                    </td>
                    <td class="style4">
                        <asp:Button ID="Button2" runat="server" onclick="Button2_Click" Text="Add" />
                    </td>
                </tr>
            </table>
            </asp:View>
            <asp:View ID="View3" runat="server">
            <table class="style1">
        <tr>
            <td class="style18">
                </td>
            <td class="style19">
                <asp:Label ID="Label3" runat="server" ForeColor="White" Text="Category Name"></asp:Label>
            </td>
            <td class="style20">
                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style17">
                &nbsp;</td>
            <td class="style16">
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            </td>
            <td class="style7">
                <asp:Button ID="Button4" runat="server" onclick="Button4_Click" Text="Update" />
                &nbsp;
                <asp:Button ID="Button3" runat="server" Text="Delete" onclick="Button3_Click" />
            </td>
        </tr>
    </table>
                        </asp:View>
    </asp:MultiView>
    
    
    
</asp:Content>

