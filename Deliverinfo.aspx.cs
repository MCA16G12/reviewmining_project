﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Deliverinfo : System.Web.UI.Page
{
    dboperation ob = new dboperation();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MultiView1.SetActiveView(View1);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT order_tbl.order_id, cart.product_id,cart.quantity, product.product_name, order_tbl.user_id, order_tbl.amount, order_tbl.date, order_tbl.status FROM order_tbl INNER JOIN cart ON order_tbl.order_id = cart.order_id INNER JOIN product ON cart.product_id = product.product_id and order_tbl.status='confirmed'";
            DataGrid1.DataSource = ob.view(cmd);
            DataGrid1.DataBind();
        }
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        MultiView1.SetActiveView(View2);
        TextBox1.Text = e.Item.Cells[2].Text;
        TextBox2.Text = e.Item.Cells[5].Text;
        TextBox3.Text = e.Item.Cells[3].Text;
      
        Label1.Text = e.Item.Cells[0].Text;
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "update order_tbl set status='delivered' where order_id='"+Label1.Text+"'";
        ob.execute(cmd);
        Response.Write("<script>alert('Order Delivered...!!')</script>");
        MultiView1.SetActiveView(View1);
        cmd.CommandText = "SELECT order_tbl.order_id, cart.product_id,cart.quantity, product.product_name, order_tbl.user_id, order_tbl.amount, order_tbl.date, order_tbl.status FROM order_tbl INNER JOIN cart ON order_tbl.order_id = cart.order_id INNER JOIN product ON cart.product_id = product.product_id and order_tbl.status='pending'";
        DataGrid1.DataSource = ob.view(cmd);
        DataGrid1.DataBind();

      
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "delete from order_tbl where order_id='" + Label1.Text + "'";
        ob.execute(cmd);
        Response.Write("<script>alert('deleted')</script>");
        MultiView1.SetActiveView(View1);
        cmd.CommandText = "SELECT order_tbl.order_id, cart.product_id,cart.quantity, product.product_name, order_tbl.user_id, order_tbl.amount, order_tbl.date, order_tbl.status FROM order_tbl INNER JOIN cart ON order_tbl.order_id = cart.order_id INNER JOIN product ON cart.product_id = product.product_id and order_tbl.status='pending'";
        DataGrid1.DataSource = ob.view(cmd);
        DataGrid1.DataBind();


    }
    protected void DataGrid1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}