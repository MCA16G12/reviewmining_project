﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class category_mngmnt : System.Web.UI.Page
{
    dboperation ob = new dboperation();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View1);
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select max(category_id) from category";
        id = ob.max_id(cmd);

        cmd.CommandText = "select * from category";
        DataGrid1.DataSource = ob.view(cmd);
        DataGrid1.DataBind();
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "insert into category values('" + id + "','" + TextBox1.Text + "')";
        ob.execute(cmd);

        Response.Write("<script>alert('success')</script>");

        cmd.CommandText = "select max(category_id) from category";
        id = ob.max_id(cmd);

        TextBox1.Text = "";

        MultiView1.SetActiveView(View1);
        cmd.CommandText = "select * from category";
        DataGrid1.DataSource = ob.view(cmd);
        DataGrid1.DataBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View2);
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        MultiView1.SetActiveView(View3);
        TextBox2.Text=e.Item.Cells[1].Text;
        Label1.Text=e.Item.Cells[0].Text;
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "update category set category_name='" + TextBox2.Text + "' where category_id='" + Label1.Text + "'";
        ob.execute(cmd);
        Response.Write("<script>alert('updated')</script>");


        MultiView1.SetActiveView(View1);
        cmd.CommandText = "select * from category";
        DataGrid1.DataSource = ob.view(cmd);
        DataGrid1.DataBind();
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "delete from category   where category_id='" + Label1.Text + "'";
        ob.execute(cmd);
        Response.Write("<script>alert('deleted')</script>");


        MultiView1.SetActiveView(View1);
        cmd.CommandText = "select * from category";
        DataGrid1.DataSource = ob.view(cmd);
        DataGrid1.DataBind();
    }
}