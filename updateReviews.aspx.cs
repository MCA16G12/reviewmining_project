﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Net;
using System.IO;
using System.Data.SqlClient;

public partial class _Default : System.Web.UI.Page
{
    dboperation db = new dboperation();
    List<string> kw = new List<string>();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        DropDownList1.DataSource = rivews();
        DropDownList1.DataValueField = "id";
        DropDownList1.DataTextField = "name";

        DropDownList1.DataBind();
    }

    public DataTable rivews()
    {
        string d = TextBox1.Text;
        string ur = "http://www.gsmarena.com/results.php3?sQuickSearch=yes&sName=" + TextBox1.Text;
        string result = "";

        HttpWebRequest webrequest = (HttpWebRequest)HttpWebRequest.Create(ur);
        webrequest.Method = "GET";
        webrequest.ContentLength = 0;

        WebResponse response = webrequest.GetResponse();

        using (StreamReader stream = new StreamReader(response.GetResponseStream()))
        {
            result = stream.ReadToEnd();


        }
        DataTable dt = new DataTable();
        dt.Columns.Add("name");
        dt.Columns.Add("id");

        result = result.Substring(result.IndexOf("review-body"));
        result = result.Substring(result.IndexOf("<li>"));

        string[] arr = new string[] { "<li>" };
        string[] resarray = result.Split(arr, StringSplitOptions.RemoveEmptyEntries);

        ArrayList alist = new ArrayList();

        foreach (string ent in resarray)
        {
            if (ent.Contains("img src"))
            {
                string tmp = ent.Substring(ent.IndexOf("=") + 2, ent.IndexOf(".") - 5);
                alist.Add(tmp);

                string[] lii = tmp.Split('-');
                dt.Rows.Add(lii[0], tmp);
            }
        }
        Session["drop"] = dt;
        return dt; ;
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        string pid = DropDownList1.SelectedValue;
        string nam = DropDownList1.SelectedItem.Text;
        GetCity(pid,nam);
        //TextBox2.Text = GetCity(pid, nam);
        //DataGrid1.DataSource = Session["rev"];
        //DataGrid1.DataBind();
    }
    
    public string GetCity(string pid,string nam)
    {
        string rv = "";
        int rp = 0, rn = 0;
        DataTable datat = new DataTable();
        datat.Columns.Add("Cmnts");
        //"ctl00$ContentPlaceHolder1$DropDownList1"
        string url = "http://www.gsmarena.com/" + nam + "-reviews-" + pid.Split('-')[1];

        HttpWebRequest webrequest = (HttpWebRequest)HttpWebRequest.Create(url);
        webrequest.Method = "GET";
        webrequest.ContentLength = 0;

        WebResponse response = webrequest.GetResponse();
        string result = "";

        using (StreamReader stream = new StreamReader(response.GetResponseStream()))
        {
            result = stream.ReadToEnd();

            result = result.Substring(result.IndexOf("all-opinions"));
            result = result.Substring(0, result.IndexOf("sub-header"));
            //<p class="uopin">
            string[] arr = new string[] { "uopin" };
            string[] ostring = result.Split(arr, StringSplitOptions.RemoveEmptyEntries);
            //string[] arr1 = new string[] { "</p>" };
            //</p>

            for (int i = 1; i < ostring.Length - 1; i++)
            {
                string cmmnt = ostring[i].Substring(0, ostring[i].IndexOf("</p>"));
                //if (cmmnt.Contains("a href"))
                //{ 
                //}

                //datat.Rows.Add(cmmnt);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select max(revID) from liveReview";
                id = db.max_id(cmd);

                cmd.CommandText = "insert into liveReview values('"+id+"','"+nam+"','"+cmmnt+"')";
                db.execute(cmd);
                //ListBox1.Items.Add(cmmnt);
            }
        }
        Session["rev"] = datat;



        //SqlCommand c1 = new SqlCommand();
        //c1.CommandText = "slct_stm";
        //DataTable dt1 = new DataTable();
        //dt1 = db.getdata(c1);
        //SqlCommand cmd = new SqlCommand();
        //DataColumn dcc1 = new DataColumn("rev");
        //dt1.Columns.Add(dcc1);

        if (datat.Rows.Count > 0)
        {
            int flag = 0;
            int pos = 0;
            int neg = 0;
            //SqlCommand cmd9 = new SqlCommand();
            //cmd.CommandText = "select review from  Add_review where product='"+TextBox1.Text+"'";
            //DataTable dt = db.getdata(cmd);
            foreach (DataRow dr in datat.Rows)
            {
                string s = dr[0].ToString().Replace(".", " ");
                string[] word = s.Split(' ');
                for (int i = 0; i < word.Count(); i++)
                {

                    SqlCommand cmd1 = new SqlCommand();
                    cmd1.CommandText = "select stem from A_stem ";
                    DataTable dtstem = db.view(cmd1);
                    foreach (DataRow dr1 in dtstem.Rows)
                    {
                        if (dr1[0].ToString() == word[i])
                        {
                            flag = 1;
                            break;
                        }

                        if (flag == 0)
                        {
                            kw.Add(word[i]);

                        }
                    }

                    SqlCommand cmd2 = new SqlCommand();
                    cmd2.CommandText = "select plsa,Value from A_plsa ";
                    DataTable dt4 = db.view(cmd2);
                    int val = 0;
                    foreach (string str in kw)
                    {
                        foreach (DataRow dr4 in dt4.Rows)
                        {
                            if (str == dr4[0].ToString())
                            {
                                val += Convert.ToInt32(dr4[1].ToString());
                                break;

                            }
                        }
                    }
                    if (val > 0)
                    {
                        pos++;
                    }
                    else
                    {
                        neg++;



                    }


                }
            }
            Session["n"] = 250;
            Session["p"] = neg;

            if (pos > neg)
            {
                rv = "GOOD";
            }
            else if (pos < neg)
            {
                rv = "BAD";

            }

            else
            {
                rv = "NEUTRAL";
            }

        }
        return rv;
    }
}