﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class product_mangmnt : System.Web.UI.Page
{
    dboperation ob = new dboperation();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MultiView1.SetActiveView(View1);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select max(product_id) from product";
            id = ob.max_id(cmd);


            cmd.CommandText = "SELECT product.product_id, product.product_name, product.category_id, category.category_name, product.quantity, product.price FROM category INNER JOIN product ON category.category_id = product.category_id";
            DataGrid1.DataSource = ob.view(cmd);
            DataGrid1.DataBind();



            cmd.CommandText = "select * from category";
            DropDownList1.DataSource = ob.view(cmd);
            DropDownList1.DataTextField = "category_name";
            DropDownList1.DataValueField = "category_id";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, "Select");

            cmd.CommandText = "select * from category";
            DropDownList2.DataSource = ob.view(cmd);
            DropDownList2.DataTextField = "category_name";
            DropDownList2.DataValueField = "category_id";
            DropDownList2.DataBind();
            DropDownList2.Items.Insert(0, "Select");
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
         SqlCommand cmd = new SqlCommand();
         cmd.CommandText = "insert into product values('" + id + "','" + TextBox1.Text + "','" + DropDownList1.SelectedValue +"','" + TextBox3.Text + "','" + TextBox4.Text + "')";
        ob.execute(cmd);

        Response.Write("<script>alert('success')</script>");

        MultiView1.SetActiveView(View1);
        cmd.CommandText = "SELECT product.product_id, product.product_name, product.category_id, category.category_name, product.quantity, product.price FROM category INNER JOIN product ON category.category_id = product.category_id";
        DataGrid1.DataSource = ob.view(cmd);
        DataGrid1.DataBind();
    }

protected void Button2_Click(object sender, EventArgs e)
{
    MultiView1.SetActiveView(View2);

}
protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
{
    MultiView1.SetActiveView(View3);
    TextBox5.Text = e.Item.Cells[2].Text;
    DropDownList2.SelectedValue= e.Item.Cells[3].Text;
    TextBox7.Text = e.Item.Cells[4].Text;
    TextBox8.Text = e.Item.Cells[5].Text;
    Label1.Text = e.Item.Cells[0].Text;
}
protected void Button3_Click(object sender, EventArgs e)
{
    SqlCommand cmd = new SqlCommand();
    cmd.CommandText = "update product set product_name='"+TextBox5.Text+"',category_id='" + DropDownList2.SelectedValue+ "',quantity='"+TextBox7.Text+"',price='"+TextBox8.Text+"' where product_id='" + Label1.Text + "'";
    ob.execute(cmd);
    Response.Write("<script>alert('updated')</script>");


    MultiView1.SetActiveView(View1);
    cmd.CommandText = "SELECT product.product_id, product.product_name, product.category_id, category.category_name, product.quantity, product.price FROM category INNER JOIN product ON category.category_id = product.category_id";
    DataGrid1.DataSource = ob.view(cmd);
    DataGrid1.DataBind();
}


protected void Button4_Click1(object sender, EventArgs e)
{
    SqlCommand cmd = new SqlCommand();
    cmd.CommandText = "delete from product  where product_id='" + Label1.Text + "'";
    ob.execute(cmd);
    Response.Write("<script>alert('deleted')</script>");


    MultiView1.SetActiveView(View1);
    cmd.CommandText = "SELECT product.product_id, product.product_name, product.category_id, category.category_name, product.quantity, product.price FROM category INNER JOIN product ON category.category_id = product.category_id";
    DataGrid1.DataSource = ob.view(cmd);
    DataGrid1.DataBind();
}

}