﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="product_mangmnt.aspx.cs" Inherits="product_mangmnt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            height: 91px;
        }
        .style4
        {
            height: 102px;
            width: 190px;
        }
        .style5
        {
            height: 156px;
        }
        .style8
        {
            height: 156px;
            width: 156px;
        }
        .style9
        {
            width: 156px;
        }
        .style10
        {
            height: 88px;
            width: 146px;
        }
        .style11
        {
            height: 102px;
            width: 146px;
        }
        .style14
        {
            height: 88px;
            width: 190px;
        }
        .style15
        {
            height: 104px;
            width: 153px;
        }
        .style16
        {
            height: 104px;
            width: 180px;
        }
        .style17
        {
            height: 104px;
        }
        .style18
        {
            height: 89px;
            width: 153px;
        }
        .style19
        {
            height: 89px;
            width: 180px;
        }
        .style20
        {
            height: 89px;
        }
        .style21
        {
            height: 90px;
            width: 153px;
        }
        .style22
        {
            height: 90px;
            width: 180px;
        }
        .style23
        {
            height: 90px;
        }
        .style24
        {
            height: 98px;
            width: 153px;
        }
        .style25
        {
            height: 98px;
            width: 180px;
        }
        .style26
        {
            height: 98px;
        }
        .style27
        {
            height: 95px;
            width: 146px;
        }
        .style28
        {
            height: 95px;
            width: 190px;
        }
        .style29
        {
            height: 95px;
        }
        .style30
        {
            height: 76px;
            width: 146px;
        }
        .style31
        {
            height: 76px;
            width: 190px;
        }
        .style32
        {
            height: 76px;
        }
        .style33
        {
            height: 96px;
            width: 146px;
        }
        .style34
        {
            height: 96px;
            width: 190px;
        }
        .style35
        {
            height: 96px;
        }
        .style36
        {
            height: 83px;
            width: 146px;
        }
        .style37
        {
            height: 83px;
            width: 190px;
        }
        .style38
        {
            height: 83px;
        }
        .style39
        {
            height: 88px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server" onload="Page_Load">
        
            <table class="style1">
                <tr>
                    <td class="style8">
                        &nbsp;</td>
                    <td class="style5">
                        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                            CellPadding="4" ForeColor="#333333" GridLines="None" Height="134px" 
                            onitemcommand="DataGrid1_ItemCommand" Width="559px">
                            <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <asp:BoundColumn DataField="product_id" HeaderText="Product_Id" Visible="False">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="category_name" HeaderText="Category Name">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="product_name" HeaderText="Product_Name">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="category_id" HeaderText="Category_Id" 
                                    Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="quantity" HeaderText="Quantity"></asp:BoundColumn>
                                <asp:BoundColumn DataField="price" HeaderText="Price"></asp:BoundColumn>
                                <asp:ButtonColumn HeaderText="View" Text="view"></asp:ButtonColumn>
                            </Columns>
                            <EditItemStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        </asp:DataGrid>
                    </td>
                    <td class="style5">
                        </td>
                </tr>
                <tr>
                    <td class="style9">
                        &nbsp;</td>
                    <td>
                        <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                            Text="Add New Product" />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style9">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        
        </asp:View>
        <asp:View ID="View2" runat="server">
        <table class="style1">
        <tr>
            <td class="style15">
                </td>
            <td class="style16">
                <asp:Label ID="Label2" runat="server" ForeColor="White" Text="Category Name"></asp:Label>
            </td>
            <td class="style17">
                <asp:DropDownList ID="DropDownList1" runat="server" >
                   
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="DropDownList1" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style18">
                </td>
            <td class="style19">
                <asp:Label ID="Label3" runat="server" ForeColor="White" Text="Product Name"></asp:Label>
            </td>
            <td class="style20">
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="TextBox1" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style21">
                </td>
            <td class="style22">
                <asp:Label ID="Label4" runat="server" ForeColor="White" Text="Quantity"></asp:Label>
            </td>
            <td class="style23">
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="TextBox3" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style24">
                </td>
            <td class="style25">
                <asp:Label ID="Label5" runat="server" ForeColor="White" Text="Price"></asp:Label>
            </td>
            <td class="style26">
                <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="TextBox4" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style21">
                </td>
            <td class="style22">
            </td>
            <td class="style23">
                <asp:Button ID="Button1" runat="server" Height="29px" Text="Add" Width="83px" 
                    onclick="Button1_Click" />
            </td>
        </tr>
    </table>
        </asp:View>
        <asp:View ID="View3" runat="server">
        <table class="style1">
        <tr>
            <td class="style27">
                </td>
            <td class="style28">
                <asp:Label ID="Label6" runat="server" ForeColor="White" Text="Category"></asp:Label>
            </td>
            <td class="style29">
                <asp:DropDownList ID="DropDownList2" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style30">
                </td>
            <td class="style31">
                <asp:Label ID="Label7" runat="server" ForeColor="White" Text="Product Name"></asp:Label>
            </td>
            <td class="style32">
                <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style33">
                </td>
            <td class="style34">
                <asp:Label ID="Label8" runat="server" ForeColor="White" Text="Quantity"></asp:Label>
            </td>
            <td class="style35">
                <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style36">
                </td>
            <td class="style37">
                <asp:Label ID="Label9" runat="server" ForeColor="White" Text="Price"></asp:Label>
            </td>
            <td class="style38">
                <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style10">
            </td>
            <td class="style14">
                <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
            </td>
            <td class="style39">
                <asp:Button ID="Button3" runat="server" Text="Update" onclick="Button3_Click" 
                    Height="29px" />
                &nbsp;<asp:Button ID="Button4" runat="server" Text="Delete" 
                    onclick="Button4_Click1" />
            </td>
        </tr>
    </table>
        </asp:View>
    </asp:MultiView>
    
</asp:Content>

