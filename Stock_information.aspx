﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="Stock_information.aspx.cs" Inherits="Stock_information" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 177px;
        }
        .style3
        {
            width: 177px;
            height: 43px;
        }
        .style4
        {
            height: 43px;
        }
        .style5
        {
            width: 177px;
            height: 42px;
        }
        .style6
        {
            height: 42px;
        }
        .style7
        {
            width: 177px;
            height: 41px;
        }
        .style8
        {
            height: 41px;
        }
        .style9
        {
            width: 213px;
        }
        .style10
        {
            width: 215px;
            height: 43px;
        }
        .style11
        {
            width: 215px;
            height: 42px;
        }
        .style12
        {
            width: 215px;
            height: 41px;
        }
        .style13
        {
            width: 215px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <table class="style1">
                <tr>
                    <td class="style9">
                        &nbsp;</td>
                    <td>
                        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                            CellPadding="4" ForeColor="#333333" GridLines="None" Height="131px" 
                            onitemcommand="DataGrid1_ItemCommand" Width="537px">
                            <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <asp:BoundColumn DataField="product_id" HeaderText="Product_Id">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="product_name" HeaderText="Product_Name">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="category_id" HeaderText="Category_Id">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="quantity" HeaderText="Quantity"></asp:BoundColumn>
                                <asp:BoundColumn DataField="price" HeaderText="Price" Visible="False">
                                </asp:BoundColumn>
                                <asp:ButtonColumn HeaderText="View" Text="view"></asp:ButtonColumn>
                            </Columns>
                            <EditItemStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        </asp:DataGrid>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style9">
                        &nbsp; &nbsp; &nbsp;</td>
                    <td>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View2" runat="server">
        <table class="style1">
        <tr>
            <td class="style10">
                &nbsp;</td>
            <td class="style3">
                <asp:Label ID="Label2" runat="server" ForeColor="White" Text="Product Name"></asp:Label>
            </td>
            <td class="style4">
                <asp:TextBox ID="TextBox1" runat="server" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style11">
                &nbsp;</td>
            <td class="style5">
                <asp:Label ID="Label3" runat="server" ForeColor="White" Text="Category"></asp:Label>
            </td>
            <td class="style6">
                <asp:TextBox ID="TextBox4" runat="server" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style12">
                &nbsp;</td>
            <td class="style7">
                <asp:Label ID="Label4" runat="server" ForeColor="White" Text="Quantity"></asp:Label>
            </td>
            <td class="style8">
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="TextBox3" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style13">
                &nbsp;</td>
            <td class="style2">
                <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
            </td>
            <td>
                <asp:Button ID="Button2" runat="server" Text="update" onclick="Button2_Click" />
            </td>
        </tr>
    </table>
        </asp:View>
    </asp:MultiView>
    
    
</asp:Content>

