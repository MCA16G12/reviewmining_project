﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class viewcom_postreply : System.Web.UI.Page
{
    dboperation db = new dboperation();
    SqlCommand cmd = new SqlCommand();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View1);
        cmd.CommandText = "select cid,complaint,uid from complaint where reply='pending'";
        DataGrid1.DataSource = db.view(cmd);
        DataGrid1.DataBind();
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        MultiView1.SetActiveView(View2);
        id = Convert.ToInt32(e.Item.Cells[0].Text);
        TextBox1.Text = e.Item.Cells[1].Text;
        TextBox2.Text = e.Item.Cells[2].Text;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        cmd.CommandText = "update complaint set reply='" + TextBox3.Text + "'where cid= '" + id + "'";
        db.execute(cmd);
    }
}