﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="Deliverinfo.aspx.cs" Inherits="Deliverinfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 328px;
        }
        .style6
        {
            width: 88px;
        }
        .style7
        {
            width: 209px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="style1">
        <tr>
            <td>
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table class="nav-justified">
                            <tr>
                                <td class="style3">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="style3">
                                    &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                    <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                                        CellPadding="4" ForeColor="#333333" GridLines="None" Height="192px" 
                                        onitemcommand="DataGrid1_ItemCommand" Width="683px">
                                        <AlternatingItemStyle BackColor="White" />
                                        <Columns>
                                            <asp:BoundColumn DataField="order_id" HeaderText="Order_Id"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="user_id" HeaderText="User_Id"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="product_name" HeaderText="Product_Name">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="date" HeaderText="Date"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="amount" HeaderText="Amount"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="quantity" HeaderText="Quantity" Visible="False">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="status" HeaderText="Status" Visible="False">
                                            </asp:BoundColumn>
                                            <asp:ButtonColumn HeaderText="View" Text="view"></asp:ButtonColumn>
                                        </Columns>
                                        <EditItemStyle BackColor="#2461BF" />
                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <ItemStyle BackColor="#EFF3FB" />
                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    </asp:DataGrid>
                                </td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="style3">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </table>
                        
                        
                        
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table class="style1">
                            <tr>
                                <td class="style6">
                                    &nbsp;</td>
                                <td class="style7">
                                    <asp:Label ID="Label2" runat="server" ForeColor="White" Text="Produc Name"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style6">
                                    &nbsp;</td>
                                <td class="style7">
                                    <asp:Label ID="Label3" runat="server" ForeColor="White" Text="Quantity"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style6">
                                    &nbsp;</td>
                                <td class="style7">
                                    <asp:Label ID="Label4" runat="server" ForeColor="White" Text="Date"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style6">
                                    &nbsp;</td>
                                <td class="style7">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="style6">
                                    &nbsp;</td>
                                <td class="style7">
                                    <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
                                </td>
                                <td>
                                    <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                                        Text="Deliver" />
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="Button3" runat="server" onclick="Button3_Click" Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <br />
                        <br />
    </asp:View>
                </asp:MultiView>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    
</asp:Content>

